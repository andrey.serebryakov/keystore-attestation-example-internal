package com.google.attestationexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.security.spec.ECGenParameterSpec;

import static android.security.keystore.KeyProperties.KEY_ALGORITHM_EC;

public class AttestationActivity extends AppCompatActivity {
    private static final String TAG = AttestationActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attestation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab_attest = findViewById(R.id.fab_attest);
        fab_attest.setOnClickListener(this::doIt);

        FloatingActionButton fab_email = findViewById(R.id.fab_email);
        fab_email.setOnClickListener(this::emailIt);

        ((TextView) findViewById(R.id.textview)).setMovementMethod(new ScrollingMovementMethod());
    }

    AttestationTest lastAttestationTest = null;

    private void doIt(final View v) {
        TextView textView = findViewById(R.id.textview);
        textView.setText("");
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);

            AttestationTest.AttestationTestParamsBuilder paramBuilder =
                    new AttestationTest.AttestationTestParamsBuilder()
                            .setKeyStoreProvider("AndroidKeyStore")
                            .setKeyAlgorithm(KEY_ALGORITHM_EC)
                            .setKeyAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1"))
                            .setChallenge("Ch@lL3N9E")
                            .setSignatureAlgorithm("SHA256WithECDSA")
                            .setSignatureText("Hello, World!")
                            .setDisplayMetrics(displayMetrics);

            lastAttestationTest = new AttestationTest(
                    getApplicationContext().getPackageName(),
                    (values) -> {
                        for (String value : values) textView.append(value);
                    },
                    () -> v.setEnabled(false),
                    (aVoid) -> v.setEnabled(true)
            );
            lastAttestationTest.execute(paramBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void emailIt(final View v) {
        TextView textView = findViewById(R.id.textview);

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_SUBJECT, "Attestation results: " + getApplicationContext().getPackageName());
        i.putExtra(Intent.EXTRA_TEXT, textView.getText());
        try {
            startActivity(Intent.createChooser(i, "Send text to:"));
        } catch (android.content.ActivityNotFoundException e) {
            Toast.makeText(this, "There are no email clients installed :/", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_attestation, menu);

        if (!menu.hasVisibleItems()) return true;

        for (int i = 0; i < menu.size(); ++i) {
            MenuItem menuItem = menu.getItem(i);
            if (!menuItem.isVisible()) continue;

            switch (menuItem.getItemId()) {
                case R.id.action_settings:
                    menuItem.setOnMenuItemClickListener(menuItemClicked -> {
                        Log.d(TAG, "[" + menuItemClicked.getTitle() + "] clicked!");

                        return true;
                    });
                    break;
                default:
                    Log.d(TAG, "Menu: no listener for option [" + menuItem.getTitle() + "]");
                    break;
            }
        }
        return true;
    }
}
