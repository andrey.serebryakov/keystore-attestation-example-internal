package com.google.attestationexample;

import android.os.AsyncTask;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.ECGenParameterSpec;
import java.util.Arrays;

import static android.security.keystore.KeyProperties.DIGEST_SHA256;
import static android.security.keystore.KeyProperties.KEY_ALGORITHM_EC;

/**
 * AttestationTest generates an EC Key pair, with attestation, and displays the result in the
 * TextView provided to its constructor.
 */
public class AttestationTest extends AsyncTask<AttestationTest.AttestationTestParams, String, Void> {
    private static final float ORIGINATION_TIME_OFFSET_MINUTES = 2.5f;
    private static final float CONSUMPTION_TIME_OFFSET_MINUTES = 3.0f;

    private static final int KEY_USAGE_DIGITAL_SIGNATURE_BIT_OFFSET = 0;
    private static final int KEY_USAGE_KEY_ENCIPHERMENT_BIT_OFFSET = 2;
    private static final int KEY_USAGE_DATA_ENCIPHERMENT_BIT_OFFSET = 3;


    public String packageName;
    private final AttestationTestListener valueListener;
    private final AttestationTestPreExecuteListener preExecuteListener;
    private final AttestationTestPostExecuteListener postExecuteListener;

    AttestationTest(
            String packageName,
            @NotNull AttestationTestListener listener,
            AttestationTestPreExecuteListener preExecuteListener,
            AttestationTestPostExecuteListener postExecuteListener
    ) {
        this.valueListener = listener;
        this.preExecuteListener = preExecuteListener;
        this.postExecuteListener = postExecuteListener;

        this.packageName = packageName;
    }

    private static final String GOOGLE_ROOT_CERTIFICATE =
            "-----BEGIN CERTIFICATE-----\n"
                    + "MIIFYDCCA0igAwIBAgIJAOj6GWMU0voYMA0GCSqGSIb3DQEBCwUAMBsxGTAXBgNV"
                    + "BAUTEGY5MjAwOWU4NTNiNmIwNDUwHhcNMTYwNTI2MTYyODUyWhcNMjYwNTI0MTYy"
                    + "ODUyWjAbMRkwFwYDVQQFExBmOTIwMDllODUzYjZiMDQ1MIICIjANBgkqhkiG9w0B"
                    + "AQEFAAOCAg8AMIICCgKCAgEAr7bHgiuxpwHsK7Qui8xUFmOr75gvMsd/dTEDDJdS"
                    + "Sxtf6An7xyqpRR90PL2abxM1dEqlXnf2tqw1Ne4Xwl5jlRfdnJLmN0pTy/4lj4/7"
                    + "tv0Sk3iiKkypnEUtR6WfMgH0QZfKHM1+di+y9TFRtv6y//0rb+T+W8a9nsNL/ggj"
                    + "nar86461qO0rOs2cXjp3kOG1FEJ5MVmFmBGtnrKpa73XpXyTqRxB/M0n1n/W9nGq"
                    + "C4FSYa04T6N5RIZGBN2z2MT5IKGbFlbC8UrW0DxW7AYImQQcHtGl/m00QLVWutHQ"
                    + "oVJYnFPlXTcHYvASLu+RhhsbDmxMgJJ0mcDpvsC4PjvB+TxywElgS70vE0XmLD+O"
                    + "JtvsBslHZvPBKCOdT0MS+tgSOIfga+z1Z1g7+DVagf7quvmag8jfPioyKvxnK/Eg"
                    + "sTUVi2ghzq8wm27ud/mIM7AY2qEORR8Go3TVB4HzWQgpZrt3i5MIlCaY504LzSRi"
                    + "igHCzAPlHws+W0rB5N+er5/2pJKnfBSDiCiFAVtCLOZ7gLiMm0jhO2B6tUXHI/+M"
                    + "RPjy02i59lINMRRev56GKtcd9qO/0kUJWdZTdA2XoS82ixPvZtXQpUpuL12ab+9E"
                    + "aDK8Z4RHJYYfCT3Q5vNAXaiWQ+8PTWm2QgBR/bkwSWc+NpUFgNPN9PvQi8WEg5Um"
                    + "AGMCAwEAAaOBpjCBozAdBgNVHQ4EFgQUNmHhAHyIBQlRi0RsR/8aTMnqTxIwHwYD"
                    + "VR0jBBgwFoAUNmHhAHyIBQlRi0RsR/8aTMnqTxIwDwYDVR0TAQH/BAUwAwEB/zAO"
                    + "BgNVHQ8BAf8EBAMCAYYwQAYDVR0fBDkwNzA1oDOgMYYvaHR0cHM6Ly9hbmRyb2lk"
                    + "Lmdvb2dsZWFwaXMuY29tL2F0dGVzdGF0aW9uL2NybC8wDQYJKoZIhvcNAQELBQAD"
                    + "ggIBACDIw41L3KlXG0aMiS//cqrG+EShHUGo8HNsw30W1kJtjn6UBwRM6jnmiwfB"
                    + "Pb8VA91chb2vssAtX2zbTvqBJ9+LBPGCdw/E53Rbf86qhxKaiAHOjpvAy5Y3m00m"
                    + "qC0w/Zwvju1twb4vhLaJ5NkUJYsUS7rmJKHHBnETLi8GFqiEsqTWpG/6ibYCv7rY"
                    + "DBJDcR9W62BW9jfIoBQcxUCUJouMPH25lLNcDc1ssqvC2v7iUgI9LeoM1sNovqPm"
                    + "QUiG9rHli1vXxzCyaMTjwftkJLkf6724DFhuKug2jITV0QkXvaJWF4nUaHOTNA4u"
                    + "JU9WDvZLI1j83A+/xnAJUucIv/zGJ1AMH2boHqF8CY16LpsYgBt6tKxxWH00XcyD"
                    + "CdW2KlBCeqbQPcsFmWyWugxdcekhYsAWyoSf818NUsZdBWBaR/OukXrNLfkQ79Iy"
                    + "ZohZbvabO/X+MVT3rriAoKc8oE2Uws6DF+60PV7/WIPjNvXySdqspImSN78mflxD"
                    + "qwLqRBYkA3I75qppLGG9rp7UCdRjxMl8ZDBld+7yvHVgt1cVzJx9xnyGCC23Uaic"
                    + "MDSXYrB4I4WHXPGjxhZuCuPBLTdOLU8YRvMYdEvYebWHMpvwGCF6bAx3JBpIeOQ1"
                    + "wDB5y0USicV3YgYGmi+NZfhA4URSh77Yd6uuJOJENRaNVTzk\n"
                    + "-----END CERTIFICATE-----";

    @Override
    protected Void doInBackground(AttestationTestParams... params) {
        try {
            if (null == params || params.length <= 0) {
                throw new Exception("No params set");
            }

            testAttestation(params[0]);
        } catch (Exception e) {
            StringWriter s = new StringWriter();
            e.printStackTrace(new PrintWriter(s));

            publishProgress(s.toString());
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        valueListener.onValues(values);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        onProgressUpdate("-- finished\n");

        if (null != postExecuteListener) {
            postExecuteListener.onPostExecute(aVoid);
        }
    }

    @Override
    protected void onPreExecute() {
        onProgressUpdate("-- started\n");

        if (null != preExecuteListener) {
            preExecuteListener.onPreExecute();
        }
    }

    private void describeDevice(AttestationTestParams params) {
        StringBuilder sb = new StringBuilder();

        sb.append("MANUFACTURER: ");
        sb.append(Build.MANUFACTURER);
        sb.append("\n");
        sb.append("BRAND: ");
        sb.append(Build.BRAND);
        sb.append("\n");
        sb.append("PRODUCT: ");
        sb.append(Build.PRODUCT);
        sb.append("\n");
        sb.append("DEVICE: ");
        sb.append(Build.DEVICE);
        sb.append("\n");
        sb.append("BOARD: ");
        sb.append(Build.BOARD);
        sb.append("\n");
        sb.append("MODEL: ");
        sb.append(Build.MODEL);
        sb.append("\n");
        sb.append("HARDWARE: ");
        sb.append(Build.HARDWARE);
        sb.append("\n");

        sb.append("BOOTLOADER: ");
        sb.append(Build.BOOTLOADER);
        sb.append("\n");
        sb.append("HOST: ");
        sb.append(Build.HOST);
        sb.append("\n");
        sb.append("Build.ID?: ");
        sb.append(Build.ID);
        sb.append("\n");
        sb.append("TYPE: ");
        sb.append(Build.TYPE);
        sb.append("\n");
        sb.append("USER: ");
        sb.append(Build.USER);
        sb.append("\n");

        sb.append("Version.BASE_OS: ");
        sb.append(Build.VERSION.BASE_OS);
        sb.append("\n");
        sb.append("Version.CODENAME: ");
        sb.append(Build.VERSION.CODENAME);
        sb.append("\n");
        sb.append("Version.INCREMENTAL: ");
        sb.append(Build.VERSION.INCREMENTAL);
        sb.append("\n");
        sb.append("Version.RELEASE(osVersion): ");
        sb.append(Build.VERSION.RELEASE);
        sb.append("\n");
        sb.append("Version.SDK_INT: ");
        sb.append(Build.VERSION.SDK_INT);
        sb.append("\n");
        sb.append("Version.SECURITY_PATCH: ");
        sb.append(Build.VERSION.SECURITY_PATCH);
        sb.append("\n");

        sb.append("Supported ABIs: ");
        for (int i = 0; i < Build.SUPPORTED_ABIS.length; ++i) {
            if (0 != i) sb.append(", ");
            sb.append(Build.SUPPORTED_ABIS[i]);
        }
        sb.append("\n");

        sb.append("Display metrics: ");
        sb.append(params.displayMetrics.widthPixels);
        sb.append(", ");
        sb.append(params.displayMetrics.heightPixels);
        sb.append("\n");

        publishProgress("\nDevice info:\n\n", sb.toString(), "\n");
    }

    private void testAttestation(AttestationTestParams params) throws Exception {

        describeDevice(params);

        KeyStore keyStore = KeyStore.getInstance(params.keyStoreProvider);
        keyStore.load(null);
        keyStore.deleteEntry(params.keyAlias);

        publishProgress("Generating key(s)...");

        DateTime startTime = DateTime.now().minusHours(12);
        Log.d("****", "Start Time is: " + startTime.toString());
        DateTime originationEnd = DateTime.now().plusSeconds(Math.round(ORIGINATION_TIME_OFFSET_MINUTES * 60));
        Log.d("****", "Origination end is: " + originationEnd.toString());
        DateTime consumptionEnd = DateTime.now().plusSeconds(Math.round(CONSUMPTION_TIME_OFFSET_MINUTES * 60));
        Log.d("****", "Consumption end is: " + consumptionEnd.toString());

        KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(params.keyAlias, params.keyPurposes)
                .setDigests(DIGEST_SHA256)
                .setAttestationChallenge(params.challenge.getBytes())
                //.setUserAuthenticationRequired(true)
                //.setUserAuthenticationValidityDurationSeconds(30)
                .setKeyValidityStart(startTime.toDate())
                .setKeyValidityForOriginationEnd(originationEnd.toDate())
                .setKeyValidityForConsumptionEnd(consumptionEnd.toDate());

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(params.keyAlgorithm, keyStore.getProvider());
        builder.setAlgorithmParameterSpec(params.keyAlgorithmParameterSpec);

        keyPairGenerator.initialize(builder.build());
        keyPairGenerator.generateKeyPair();

        publishProgress("Key pair generated\n\n");

        Certificate certificates[] = keyStore.getCertificateChain(params.keyAlias);
        publishProgress("Retrieved certificate chain of length " + certificates.length + "\n");

        for (int i = 0; i < certificates.length; ++i) {
            final byte[] derCert = certificates[i].getEncoded();
            final String pemCert = Base64.encodeToString(derCert, Base64.NO_WRAP);
            Log.d("****", pemCert);
            publishProgress("\n", "cert[" + i + "](base64): ", pemCert, "\n");
        }
        publishProgress("\n");

        try {
            verifyCertificateSignatures(certificates);
        } catch (Exception e) {
            StringWriter s = new StringWriter();
            e.printStackTrace(new PrintWriter(s));

            publishProgress("Chain verfication error\n", s.toString(), "\n");
        }

        X509Certificate attestationCert = (X509Certificate) certificates[0];
        X509Certificate secureRoot = (X509Certificate) CertificateFactory
                .getInstance("X.509").generateCertificate(
                        new ByteArrayInputStream(
                                GOOGLE_ROOT_CERTIFICATE.getBytes()));
        X509Certificate rootCert = (X509Certificate) certificates[certificates.length - 1];
        if (Arrays.equals(secureRoot.getEncoded(), rootCert.getEncoded())) {
            publishProgress(
                    "\nRoot certificate IS the Google root. This attestation is STRONG\n\n");
        } else {
            publishProgress(
                    "\nRoot certificate IS NOT the Google root. This attestation is WEAK\n\n");
        }
        printKeyUsage(attestationCert);

        Attestation attestation = new Attestation(attestationCert);
        publishProgress(attestation.toString() + "\n");

        PrivateKey key = (PrivateKey) keyStore.getKey(params.keyAlias, null);

        Signature signer = Signature.getInstance(params.signatureAlgorithm);
        signer.initSign(key);
        signer.update(params.signatureText.getBytes());
        byte[] sign = signer.sign();

        publishProgress("\n\nSuccessfully generated signature\n\ntext=", params.signatureText,
                "\n\nsignature(base64)=", Base64.encodeToString(sign, Base64.NO_WRAP), "\n\n");
    }

    private void verifyCertificateSignatures(Certificate[] certChain)
            throws GeneralSecurityException {
        for (int i = 1; i < certChain.length; ++i) {
            PublicKey pubKey = certChain[i].getPublicKey();
            try {
                certChain[i - 1].verify(pubKey);
            } catch (InvalidKeyException | CertificateException | NoSuchAlgorithmException
                    | NoSuchProviderException | SignatureException e) {
                throw new GeneralSecurityException("FAILED to verify certificate "
                        + certChain[i - 1] + " with public key " + certChain[i].getPublicKey(), e);
            }
            if (i == certChain.length - 1) {
                // Last cert is self-signed.
                try {
                    certChain[i].verify(pubKey);
                } catch (CertificateException e) {
                    throw new GeneralSecurityException(
                            "Root cert " + certChain[i] + " is not correctly self-signed", e);
                }
            }
        }
        publishProgress("Certificate chain signatures are VALID\n");
    }

    private void printKeyUsage(X509Certificate attestationCert) {
        publishProgress("Key usage:");
        if (attestationCert.getKeyUsage() == null) {
            publishProgress(" NONE\n");
            return;
        }
        if (attestationCert.getKeyUsage()[KEY_USAGE_DIGITAL_SIGNATURE_BIT_OFFSET]) {
            publishProgress(" sign");
        }
        if (attestationCert.getKeyUsage()[KEY_USAGE_DATA_ENCIPHERMENT_BIT_OFFSET]) {
            publishProgress(" encrypt_data");
        }
        if (attestationCert.getKeyUsage()[KEY_USAGE_KEY_ENCIPHERMENT_BIT_OFFSET]) {
            publishProgress(" encrypt_keys");
        }
        publishProgress("\n");
    }

    public interface AttestationTestListener {
        void onValues(String... values);
    }

    public interface AttestationTestPreExecuteListener {
        void onPreExecute();
    }

    public interface AttestationTestPostExecuteListener {
        void onPostExecute(Void aVoid);
    }

    public static class AttestationTestParams {
        private String keyStoreProvider;
        private String keyAlias;
        private int keyPurposes;
        private String keyAlgorithm;
        private AlgorithmParameterSpec keyAlgorithmParameterSpec;
        private String challenge;
        private String signatureAlgorithm;
        private String signatureText;
        private DisplayMetrics displayMetrics;

        private AttestationTestParams() {
        }

        private AttestationTestParams(
                String keyStoreProvider,
                String keyAlias, String keyAlgorithm, AlgorithmParameterSpec keyAlgorithmParameterSpec,
                int keyPurposes,
                String challenge,
                String signatureAlgorithm,
                String signatureText,
                DisplayMetrics displayMetrics
        ) {
            this.keyStoreProvider = keyStoreProvider;
            this.keyAlias = keyAlias;
            this.keyPurposes = keyPurposes;
            this.keyAlgorithm = keyAlgorithm;
            this.keyAlgorithmParameterSpec = keyAlgorithmParameterSpec;
            this.challenge = challenge;
            this.signatureAlgorithm = signatureAlgorithm;
            this.signatureText = signatureText;
            this.displayMetrics = displayMetrics;
        }
    }

    public static class AttestationTestParamsBuilder {
        public AttestationTestParamsBuilder() {
        }

        private String keyAlias = "test_key";
        private int keyPurposes = KeyProperties.PURPOSE_SIGN | KeyProperties.PURPOSE_VERIFY;

        private String keyAlgorithm = KEY_ALGORITHM_EC;
        private AlgorithmParameterSpec keyAlgorithmParameterSpec = new ECGenParameterSpec("secp256r1");
        private String keyStoreProvider = "AndroidKeyStore";
        private String challenge = "challenge";
        private String signatureAlgorithm = "SHA256WithECDSA";
        private String signatureText = "Hello";
        private DisplayMetrics displayMetrics = new DisplayMetrics();


        public AttestationTestParamsBuilder setKeyAlgorithm(final String keyAlgorithm) {
            this.keyAlgorithm = keyAlgorithm;
            return this;
        }

        public AttestationTestParamsBuilder setDisplayMetrics(final DisplayMetrics displayMetrics) {
            this.displayMetrics = displayMetrics;
            return this;
        }

        public AttestationTestParamsBuilder setSignatureAlgorithm(final String signatureAlgorithm) {
            this.signatureAlgorithm = signatureAlgorithm;
            return this;
        }

        public AttestationTestParamsBuilder setKeyStoreProvider(final String keyStoreProvider) {
            this.keyStoreProvider = keyStoreProvider;
            return this;
        }

        public AttestationTestParamsBuilder setChallenge(final String challenge) {
            this.challenge = challenge;
            return this;
        }

        public AttestationTestParamsBuilder setSignatureText(final String signatureText) {
            this.signatureText = signatureText;
            return this;
        }

        public AttestationTestParamsBuilder setKeyAlias(final String keyAlias) {
            this.keyAlias = keyAlias;
            return this;
        }

        public AttestationTestParamsBuilder setKeyAlias(final int keyPurposes) {
            this.keyPurposes = keyPurposes;
            return this;
        }

        public AttestationTestParamsBuilder setKeyAlgorithmParameterSpec(final AlgorithmParameterSpec keyAlgorithmParameterSpec) {
            this.keyAlgorithmParameterSpec = keyAlgorithmParameterSpec;
            return this;
        }

        public AttestationTestParams build() {
            return new AttestationTestParams(
                    keyStoreProvider,
                    keyAlias, keyAlgorithm, keyAlgorithmParameterSpec,
                    keyPurposes,
                    challenge,
                    signatureAlgorithm,
                    signatureText,
                    displayMetrics
            );
        }
    }

}
